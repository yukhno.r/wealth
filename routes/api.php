<?php

use Illuminate\Support\Facades\Route;

Route::post('/register', [\App\Http\Controllers\UserController::class, 'register']);
Route::post('/token', [\App\Http\Controllers\UserController::class, 'token']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/check_admin', [\App\Http\Controllers\UserController::class, 'checkAdminRole'])
        ->middleware('role:admin');
    Route::apiResources([
        '/accounts' => \App\Http\Controllers\AccountController::class,
        '/categories' => \App\Http\Controllers\CategoryController::class,
        '/transactions' => \App\Http\Controllers\TransactionController::class,
    ]);
});


