build:
	docker-compose build
up:
	docker-compose up
start:
	docker-compose up -d
down:
	docker-compose down
stop:
	docker-compose down --remove-orphans
destroy:
	docker-compose down --remove-orphans --volumes
restart:
	docker-compose down
	docker-compose build
	docker-compose up -d

exec-nginx:
	docker exec -it wealth-nginx bash
exec-php:
	docker exec -it wealth-app bash
exec-db:
	docker exec -it wealth-db bash

ls-container:
	docker ps -a
ls-image:
	docker image ls -a
ls-network:
	docker network ls
ls-volume:
	docker volume ls
