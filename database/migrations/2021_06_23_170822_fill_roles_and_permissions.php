<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FillRolesAndPermissions extends Migration
{
    private const PERMISSIONS_BY_ROLES = [
        'admin' => [
            'categories.edit',
            'categories.delete',
            'users.edit',
            'users.delete',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::PERMISSIONS_BY_ROLES as $role => $permissions) {
            $role = Role::create(['name' => $role, 'guard_name' => 'api']);
            foreach ($permissions as $permission) {
                Permission::create(['name' => $permission, 'guard_name' => 'api'])
                    ->assignRole($role);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::PERMISSIONS_BY_ROLES as $role => $permissions) {
            $role = Role::findByName($role, 'api');
            $permissionsToRevoke = Permission::whereIn('name', $permissions)
                ->where('guard_name', 'api')
                ->get();

            $role->revokePermissionTo($permissionsToRevoke);

            Permission::whereIn('name', $permissions)->delete();
            $role->delete();
        }
    }
}
