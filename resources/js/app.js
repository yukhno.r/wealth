/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import App from './components/App';
import Vue from 'vue';
import vSelect from 'vue-select'
import './bootstrap';
import store from './store';
import router from './router';

const token = 'Z9XpnyZ7eQaT3JtgxcQ3A0LOZAU072toOdRFjKcH'; // TODO: хранить токен гдето после аутентификации
axios.interceptors.request.use(
    request => {
        request.headers['Authorization'] = `Bearer ${token}`;
        return request;
    },
    error => Promise.reject(error),
)

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('v-select', vSelect);

Vue.component('transaction-item', require('./components/TransactionItem').default);
Vue.component('transactions', require('./components/Transactions').default);
Vue.component('balance', require('./components/Balance').default);

Vue.component('overview', require('./pages/mobile/Overview').default);

Vue.component('pie', require('./components/Pie').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});