import Vue from 'vue';
import VueRouter from 'vue-router';
import Overview from '../pages/mobile/Overview';
import MobileMainWrapper from '../pages/mobile/MainWrapper';

Vue.use(VueRouter);

const routes = [
    {
        path: '/mobile',
        component: MobileMainWrapper,
        children: [
            {
                path: 'overview',
                component: Overview,
            }
        ],
    },
];

export default new VueRouter({
    routes,
});