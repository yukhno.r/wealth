<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Collection;

class TransactionService
{
    public function getTransactionsByUser(User $user, ?array $filters = null): Collection
    {
        return Transaction::query()
            ->whereIn('account_id', $filters['account_id'] ?? $user->accounts()->pluck('accounts.id'))
            ->whereIn('category_id', $filters['category_id'] ?? $user->categories()->pluck('categories.id'))
            ->get();
    }
}