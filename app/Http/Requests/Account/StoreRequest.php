<?php

namespace App\Http\Requests\Account;

use App\Entities\AccountType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'account_type_key' => ['required', 'string', Rule::in(AccountType::KEYS)],
            'currency_key' => ['required', 'string'],
            'start_amount' => ['nullable', 'numeric'],
        ];
    }
}
