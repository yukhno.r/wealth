<?php

namespace App\Http\Requests;

trait Paginatable
{
    public function getPerPage(): int
    {
        return (int) request()->input('perPage', 15);
    }

    public function getPage(): int
    {
        return (int) request()->input('page', 1);
    }
}
