<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_id' => [
                'required',
                'integer',
                Rule::exists('accounts', 'id')
                    ->where('user_id', Auth::id()),
            ],
            'category_id' => [
                'nullable',
                'integer',
                Rule::exists('user_categories', 'category_id')
                    ->where('user_id', Auth::id()),
            ],
            'amount' => ['required', 'numeric'],
        ];
    }
}
