<?php

namespace App\Http\Requests\Transaction;

use App\Http\Requests\Filtratable;
use App\Http\Requests\IndexRequest as BaseIndexRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class IndexRequest extends BaseIndexRequest
{
    use Filtratable;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'filters' => ['array'],
            'filters.account_id' => [
                'array',
                Rule::exists('accounts', 'id')
                    ->where('user_id', Auth::id()),
            ],
            'filters.category_id' => [
                'array',
                Rule::exists('user_categories', 'category_id')
                    ->where('user_id', Auth::id()),
            ],
        ]);
    }
}
