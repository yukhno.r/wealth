<?php

namespace App\Http\Requests;

trait Filtratable
{
    public function getFilter(?string $name = null)
    {
        if (isset($name)) {
            return $this->get("filters.${$name}");
        }
        return $this->get('filters');
    }
}