<?php

namespace App\Http\Controllers;

use App\Http\Requests\Transaction\IndexRequest;
use App\Http\Requests\Transaction\StoreRequest;
use App\Http\Resources\Transaction as TransactionResource;
use App\Models\Transaction;
use App\Services\TransactionService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    use AuthorizesRequests;

    public function __construct()
    {
//        $this->authorizeResource(Transaction::class, 'transaction');
    }

    public function index(IndexRequest $request, TransactionService $transactionService)
    {
        $filters = $request->getFilter();

        $transactions = $transactionService->getTransactionsByUser(Auth::user(), $filters)
            ->load(['account', 'category']);

        return TransactionResource::collection($transactions);
    }

    public function show(Transaction $transaction)
    {
        return new TransactionResource($transaction);
    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $transaction = Transaction::create($input);

        return new TransactionResource($transaction);
    }

    public function update(StoreRequest $request, Transaction $transaction)
    {
        $input = $request->validated();

        $transaction->update($input);

        return new TransactionResource($transaction->fresh());
    }

    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
