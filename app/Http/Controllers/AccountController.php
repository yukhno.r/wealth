<?php

namespace App\Http\Controllers;

use App\Http\Requests\Account\StoreRequest;
use App\Http\Requests\IndexRequest;
use App\Http\Resources\Account as AccountResource;
use App\Models\Account;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function index(IndexRequest $request)
    {
        $accounts = Auth::user()
            ->accounts()
            ->when($request->get('account'), fn ($query) => $query->where('id', $request->get('account')))
            ->get();

        return AccountResource::collection($accounts);
    }

    public function show(Account $account)
    {
        return new AccountResource($account);
    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();

        $account = Auth::user()
            ->accounts()
            ->create($input);

        return new AccountResource($account);
    }

    public function update(StoreRequest $request, Account $account)
    {
        $input = $request->validated();

        $account->update($input);

        return new AccountResource($account->fresh());
    }

    public function destroy(Account $account)
    {
        $account->delete();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
