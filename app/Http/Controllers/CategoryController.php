<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\IndexRequest;
use App\Http\Resources\Category as CategoryResource;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index(IndexRequest $request)
    {
        $categories = Auth::user()
            ->categories()
            ->paginate($request->getPerPage());

        return CategoryResource::collection($categories);
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function store(StoreRequest $request)
    {
        $input = $request->validated();
        $input['created_by'] = Auth::id();

        $category = Auth::user()
            ->categories()
            ->create($input);

        return new CategoryResource($category);
    }

    public function update(StoreRequest $request, Category $category)
    {
        if (Auth::user()->cannot('update', $category)) {
            return new JsonResponse(['message' => 'Forbidden.'], Response::HTTP_FORBIDDEN);
        }

        $input = $request->validated();

        $category->update($input);

        return new CategoryResource($category->fresh());
    }

    public function destroy(Category $category)
    {
        Auth::user()->categories()->detach($category);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
