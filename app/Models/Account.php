<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'account_type_key',
        'currency_key',
        'start_amount',
    ];

    protected $casts = [
        'start_amount' => 'float',
    ];

    protected $appends = [
        'total_amount',
        'income_amount',
        'outcome_amount',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }

    public function getTotalAmountAttribute(): float
    {
        return $this->start_amount + $this->transactions()->sum('amount');
    }

    public function getIncomeAmountAttribute(): float
    {
        return $this->transactions()
            ->whereIn('category_id', Auth::user()->categories()->income()->pluck('categories.id'))
            ->sum('amount');
    }

    public function getOutcomeAmountAttribute(): float
    {
        return $this->transactions()
            ->whereIn('category_id', Auth::user()->categories()->outcome()->pluck('categories.id'))
            ->sum('amount');
    }
}
