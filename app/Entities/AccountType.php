<?php

namespace App\Entities;

class AccountType
{
    public const KEYS = [
        'simple',
        'debt',
        'saving',
    ];
}
